#!/bin/bash
IMAGE="zeromq"
TAG="latest"

docker rm -f $IMAGE

docker run -tid --restart=always \
-p 19080:8080 \
--name $IMAGE \
anilornd/$IMAGE:$TAG
